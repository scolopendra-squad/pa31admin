<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('resources/css/style.css') }}">
    <title>ProfConsult Admin</title>
</head>
<body>
<nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a href="/" class="navbar-brand">
            <span class="text-uppercase font-weight-bold">ProfConsult Admin</span>
        </a>
        <div id="navbarSupportedContent" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                @if(Auth::check())
                    <li class="nav-item nav-link active">{{Auth::user()->name}}</li>
                    <li class="nav-item active">
						<form method="POST" action="{{ route('logout') }}">
							@csrf
							<button class="btn btn-dark" type="submit">Выйти</button>
						</form>
					</li>
                @endif
<!--                 <li><a href="{{ route('register') }}" class="btn btn-dark">Зарегистрироваться</a></li> -->
                <li></li>
            </ul>
        </div>
    </div>
</nav>
<br><br>
<div class="container">
