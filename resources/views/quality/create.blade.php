@include ('header')
@if($r==1)
<form method="POST" action="/home/quality/">
    <table class="table table-dark">
        {{csrf_field()}}
        <tr>
            <td>Название Свойства</td>
            <td>
                <input name="name">
            </td>
        </tr>
        <tr>
            <td>Номер блока</td>
            <td>
                <select class="custom-select" name="block">
                    <option selected="true" value="1">1</option>
                    <option value="2">2</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Текст</td>
            <td>
                <textarea name="text"></textarea>
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/quality" class="btn btn-primary">Назад</a>
@endif
@include ('footer')