@include ('header')
@if($r==1)
<form method="POST" action="/home/quality/{{$doc->q_id}}/doc/{{$doc->id}}/">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}
        <tr>
            <td>Название документа</td>
            <td>
                <input name="name" value="{{$doc->name}}">
            </td>
        </tr>
        <tr>
            <td>Текст</td>
            <td>
                <textarea name="text" >{{$doc->text}}</textarea>
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/quality/{{$doc->q_id}}/" class="btn btn-primary">Назад</a>
@endif
@include ('footer')
