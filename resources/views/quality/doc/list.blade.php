@include ('header')
<h1>{{$qual->name}}</h1>
@if($r==1)
<a href="/home/quality/{{$id}}/doc/create" class="btn btn-warning">Создать Элемент</a>
<br><br>
<table class="table table-dark">
    <tr class="d-flex">
        <td class="col-3">Название документа</td>
        <td class="col-7">Текст</td>
        <td></td>
        <td></td>
    </tr>
    @foreach ($docs as $doc)
        <tr class="d-flex">
            <td class="col-3">{{$doc->name}}</td>
            <td class="col-7"><!-- <textarea name="text"> -->{{$doc->text}}<!-- </textarea> --></td>
            <td class="col"><a href="/home/quality/{{$doc->q_id}}/doc/{{$doc->id}}/edit"
                                 class="btn btn-outline-info">Редактировать</a></td>
            <td class="col">
                <form action="/home/quality/{{$doc->q_id}}/doc/{{$doc->id}}" method="POST">
                    {{csrf_field()}}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger" type="submit">Удалить</button>
                </form>
            </td>
        </tr>
    @endforeach
</table>
<br><br><br>
<a href="/home/quality/" class="btn btn-primary">Назад</a>
<br><br><br><br>
@endif
@include ('footer')