@include ('header')
	@if($r==1)
    <a href="/home/quality/create" class="btn btn-warning">Создать Элемент</a>
    <br><br>
    <table class="table table-dark">
        <tr>
            <td>Название свойства</td>
            <td>Номер Блока</td>
            <td>Текст</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($quals as $qual)
            <tr>
                <td class="col"><a href="/home/quality/{{$qual->id}}">{{$qual->name}}</a></td>
                <td class="col">{{$qual->block}}</td>
                <td class="col-7">{{$qual->text}}</td>
                <td class="col"><a href="/home/quality/{{$qual->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/quality/{{$qual->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    <br><br>
    <a href="/home" class="btn btn-primary">Назад</a>
    @endif
@include ('footer')

