@include ('header')
@if($r==1)
<form method="POST" action="/home/headline/{{$head->id}}">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}        
        <tr>
            <td>Заголовок</td>
            <td>
                <input name="header" value="{{$head->header}}">
            </td>
        </tr>
        <tr>
            <td>Картинка</td>
            <td>
                <input name="img" value="{{$head->img}}">
            </td>
<!--             <td>
                <button class="btn btn-success">Выбрать файл</button>            
            </td> -->
        </tr>
        <tr>
            <td>Текст</td>
            <td>
                <textarea name="text">{{$head->text}}</textarea>
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/headline" class="btn btn-primary">Назад</a>
@endif
@include ('footer')