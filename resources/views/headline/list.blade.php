@include ('header')
	@if($r==1)
    <a href="/home/headline/create" class="btn btn-warning">Создать Элемент</a>
    <br><br>
    <table class="table table-dark">
        <tr>
            <td>Заголовок</td>
            <td>Картинка</td>
            <td>Текст</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($heads as $head)
            <tr>
                <td class="col-3"><a href="/home/headline/{{$head->id}}">{{$head->header}}</a></td>
                <td class="col-3">{{$head->img}}</td>
                <td class="col-4">{{$head->text}}</td>
                <td class="col"><a href="/home/headline/{{$head->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/headline/{{$head->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    <br><br>
    <a href="/home" class="btn btn-primary">Назад</a>
    @endif
@include ('footer')