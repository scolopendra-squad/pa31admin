@include('header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (Auth::check())
                    	<p>{{Auth::user()->name}}, Вы успешно вошли!</p>
                    @else
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					<p>Перейдите к таблицам</p>
                    <table class="table table-dark">
                        <tr>
                            <td><a href="/home/headline">Новости</a></td>
                        </tr>
                        <tr>
                            <td><a href="/home/quality">Решения</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include ('footer')
