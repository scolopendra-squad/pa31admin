@include('header')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
  	@if(!Auth::check())
    <h1 class="display-3">Добро пожаловать</h1>
    <p class="lead">Для начала необходимо войти в аккаунт.</p>
    <p><a href="{{ route('login') }}" class="btn btn-dark">Войти</a></p>
    @else
    <h1 class="display-3">Добро пожаловать, {{Auth::user()->name}}</h1>
    <p class="lead">Вы уже вошли!</p>
    <p>Перейдите к таблицам</p>
    <table class="table table-dark">
      <tr>
        <td><a href="/home/headline">Новости</a></td>
      </tr>
      <tr>
        <td><a href="/home/quality">Решения</a></td>
      </tr>
    </table>
    @endif
  </div>
</div>
@include ('footer')