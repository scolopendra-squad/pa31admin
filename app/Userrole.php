<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userrole extends Model
{
    public function role(){
        return $this->belongsTo('\App\Role');
    }

}
