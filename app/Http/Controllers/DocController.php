<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        return view('quality.doc.create',['id' => $id,'r' => $r]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $q_id)
    {
        $t = new \App\Doc();
        $t->name = $request->name;
        $t->q_id = $q_id;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/quality/' . $t->q_id . '/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($q_id, $id)
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arD = \App\Doc::find($id);
        return view('quality.doc.edit',['doc' => $arD, 'r' => $r]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $q_id, $id)
    {
        $t = \App\Doc::find($id);
        $t->name = $request->name;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/quality/'.$q_id.'/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($q_id,$id)
    {
        \App\Doc::destroy($id);
        return redirect('/home/quality/'.$q_id.'/');
    }
}
