<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HeadlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arH = \App\Headline::all();
        return view('headline.list',['heads' => $arH, 'r' => $r]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        return view('headline.create',['r' => $r]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $t = new \App\Headline();
        $t->header = $request->header;
        $t->img = $request->img;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/headline/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arH = \App\Headline::find($id);
        return view('headline.edit',['head' => $arH, 'r' => $r]);    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $t = \App\Headline::find($id);
        $t->header = $request->header;
        $t->img = $request->img;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/headline/');    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Headline::destroy($id);
        return redirect('/home/headline/');    
    }
}
