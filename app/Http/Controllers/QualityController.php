<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QualityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arQ = \App\Quality::all();
        return view('quality.list',['quals' => $arQ, 'r' => $r]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        return view('quality.create',['r' => $r]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $t = new \App\Quality();
        $t->name = $request->name;
        $t->icon = $request->icon;
        $t->block = $request->block;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/quality/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arQ = \App\Quality::find($id);
        $arD = \App\Doc::where('q_id', $id)->get();
        return view('quality.doc.list', ['qual' => $arQ, 'docs' => $arD, 'id' => $id, 'r' => $r]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $r=0;
        if(Auth::check()){
            $i=Auth::id();
            $u=\App\Userrole::where('user_id', $i)->get();
            if (sizeof($u)>0){
                foreach($u as $k){
                    if($k->role_id == 1){
                        $r=1;
                    }
                }
            }
        }
        $arQ = \App\Quality::find($id);
        return view('quality.edit',['qual' => $arQ, 'r' => $r]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $t = \App\Quality::find($id);
        $t->name = $request->name;
        $t->icon = $request->icon;
        $t->block = $request->block;
        $t->text = $request->text;
        $t->save();
        $id = $t->id;

        return redirect('/home/quality/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Quality::destroy($id);
        return redirect('/home/quality/');
    }
}
